# -*- coding: utf-8 -*-
##############################################################################
#
#    pyrabbit, a Python library for easy playing with RabbitMQ
#    Copyright (C) 2013 by Javier Sancho Fernandez <jsf at jsancho dot org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import errno
import os
import pika
import time
import uuid

class TimeoutError(Exception):
    pass

class Connection(object):
    def __init__(self, host):
        self.host = host
        self.open(host=self.host)

    def open(self, host=None):
        if host is None:
            host = self.host
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=host))
        self.channel = self.connection.channel()
        self.callback_queue = None

    def close(self):
        self.channel.cancel()
        self.connection.close()

    def __getattr__(self, queue_name):
        return Queue(self, queue_name)

    def __getitem__(self, queue_name):
        return self.__getattr__(queue_name)

    def receive(self, queues=[], timeout=0):
        if len(queues) == 0:
            return None

        t_start = time.time()
        method = None
        i = 0
        delay = 0.0
        while method is None and (time.time()-t_start < timeout or timeout <= 0):
            time.sleep(delay)
            if delay < 1:
                delay += 0.01
            method, properties, body = self.channel.basic_get(queues[i])
            if i == len(queues) - 1:
                i = 0
            else:
                i += 1

        if method is None:
            raise TimeoutError(os.strerror(errno.ETIME))
        else:
            return Message(self, method, properties, body)


class Queue(object):
    def __init__(self, connection, queue_name):
        self.connection = connection
        self.queue_name = queue_name
        self.connection.channel.queue_declare(queue=self.queue_name)

    def receive(self, timeout=0):
        return self.connection.receive(queues=[self.queue_name], timeout=timeout)

    def send(self, body, wait_response=False, timeout=0):
        properties = None

        if wait_response:
            corr_id = str(uuid.uuid4())
            if self.connection.callback_queue is None:
                self.connection.callback_queue = self.connection.channel.queue_declare(exclusive=True).method.queue
            properties = pika.BasicProperties(
                reply_to=self.connection.callback_queue,
                correlation_id=corr_id,
                )
        else:
            properties = pika.BasicProperties()

        self.connection.channel.basic_publish(exchange='',
                                              routing_key=self.queue_name,
                                              properties=properties,
                                              body=body)

        response = None
        if wait_response:
            response = self.connection.receive(queues=[self.connection.callback_queue], timeout=timeout)

        return response


class Message(object):
    def __init__(self, connection, method, properties, body):
        self.connection = connection
        self.method = method
        self.properties = properties
        self.body = body
        self._ack = False

    def ack(self):
        if not self._ack:
            self.connection.channel.basic_ack(delivery_tag=self.method.delivery_tag)
            self._ack = True

    def response(self, body):
        if self.properties.reply_to:
            self.connection.channel.basic_publish(exchange='',
                                                  routing_key=self.properties.reply_to,
                                                  properties=pika.BasicProperties(correlation_id=self.properties.correlation_id),
                                                  body=body)
